﻿-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS concursos;

CREATE DATABASE IF NOT EXISTS concursos
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

--
-- Set default database
--
USE concursos;

--
-- Create table `concursantes`
--
CREATE TABLE IF NOT EXISTS concursantes (
  id int(11) NOT NULL,
  nombre varchar(12) DEFAULT NULL,
  poblacion varchar(12) DEFAULT NULL,
  provincia varchar(12) DEFAULT NULL,
  fechaNacimiento date DEFAULT NULL,
  peso int(11) NOT NULL,
  altura int(11) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 819,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `altura_index` on table `concursantes`
--
ALTER TABLE concursantes
ADD INDEX altura_index (altura);

--
-- Create index `peso_index` on table `concursantes`
--
ALTER TABLE concursantes
ADD INDEX peso_index (peso);

-- 
-- Dumping data for table concursantes
--
INSERT INTO concursantes VALUES
(1, 'Rosa', 'Laredo', 'Cantabria', '1989-07-08', 90, 180),
(2, 'Roberto', 'Potes', 'Cantabria', '1978-01-01', 98, 185),
(3, 'Laura', 'Torrelavega', 'Cantabria', '1986-12-15', 81, 187),
(4, 'Jorge', 'Santander', 'Cantabria', '1988-05-25', 85, 179),
(5, 'Luisa', 'Arandilla', 'Burgos', '1983-02-14', 102, 161),
(6, 'Pedro', 'Reinosa', 'Cantabria', '1982-12-07', 94, 164),
(7, 'Roberto', 'Santander', 'Cantabria', '1980-01-24', 76, 167),
(8, 'Luisa', 'Santander', 'Cantabria', '1981-06-20', 84, 167),
(9, 'Victor', 'Santander', 'Cantabria', '1987-07-29', 92, 179),
(10, 'Dumas', 'Santander', 'Cantabria', '1984-03-28', 83, 183),
(11, 'Jorge', 'Santander', 'Cantabria', '1983-02-14', 105, 166),
(12, 'Ana', 'Santander', 'Cantabria', '1981-02-11', 79, 170),
(13, 'Lola', 'Santander', 'Cantabria', '1989-05-19', 107, 176),
(14, 'Lolita', 'Santander', 'Cantabria', '1985-07-01', 97, 185),
(15, 'Loreto', 'Loredo', 'Cantabria', '1988-05-13', 104, 181),
(16, 'Ramon', 'Laredo', 'Cantabria', '1983-02-20', 96, 162),
(17, 'Ricardo', 'Tormes', 'Burgos', '1984-01-08', 109, 190),
(18, 'Luisa', 'Santander', 'Cantabria', '1984-03-04', 84, 186),
(19, 'Pablo', 'Santander', 'Cantabria', '1983-06-08', 81, 183),
(20, 'Ruben', 'Santander', 'Cantabria', '1989-06-21', 110, 167);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;

-- Indicar el nombre de los concursantes de Burgos.

  SELECT DISTINCT c.nombre FROM concursantes c
  WHERE c.provincia ='Burgos';

-- Indicar cuantos concursantes hay de Burgos.

  SELECT COUNT(*) Nºconsursantes FROM  concursantes c
  WHERE c.provincia ='Burgos';

-- Indicar cuantos concursantes hay de cada población.

  SELECT c. poblacion, COUNT(*) FROM  concursantes c
  GROUP BY c.poblacion;

-- Indicar las poblaciones que tienen concursantes de menos de 90 kg.

  SELECT DISTINCT  c.poblacion  FROM concursantes c
  WHERE c.peso<90
  GROUP BY c.poblacion;

-- Indicar las poblaciones que tienen más de un concursante.

  SELECT c. poblacion FROM  concursantes c
  GROUP BY c.poblacion
  HAVING COUNT(*)>1;

 -- Indicar las poblaciones que tienen más de un concursante de menos de 90 kg.
  
  SELECT c.poblacion FROM  concursantes c
  WHERE c.peso<90
  GROUP BY c.poblacion
  HAVING COUNT(*)>1;

  -- Hacer 5 consultas.

    --  1 con Where
    --  1 con having
    --  Otra con where y having
    --  1 que use funciones de totales sum avg max min con where
    --  1 que junte el where el having con los totales.

-- Consultas de otro grupo.

  -- Listar los nombres de los concursantes cuya población sea de Santander Y cuya altura mayor de 170 cms.

    SELECT DISTINCT c.nombre FROM concursantes c
    WHERE c.poblacion= 'Santander' AND c.altura<170;

  -- Indicar que población tiene más de 5 concursantes.

    SELECT DISTINCT COUNT(*), c.poblacion  FROM concursantes c
    GROUP BY c.poblacion
    HAVING COUNT(*) >5;

   -- Mostrar de cada población la altura media de los concursantes, cuya altura supere el 1,70.

    SELECT DISTINCT c.poblacion , AVG(c.altura) 
      FROM concursantes c
    WHERE c.altura>170
    GROUP BY c.poblacion;

    -- Cual es la altura del concursante más alto.

      SELECT DISTINCT MAX(c.altura)
      FROM concursantes c;
    
    -- MOstar el nombre de los concursantes cuya altura supera la altura media de Santander.

      SELECT DISTINCT AVG( c.altura)  FROM concursantes c
      WHERE c.poblacion = 'Santander';

      SELECT DISTINCT * FROM concursantes c
      WHERE c.altura>(SELECT AVG( c.altura)  FROM concursantes c
      WHERE c.poblacion = 'Santander');
